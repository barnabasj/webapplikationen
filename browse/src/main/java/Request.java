public class Request {
  private final String targetHost;
  private final String documentPath;
  private int port;

  public Request(String url, int port) {
    this.port = port;

    if (url.contains("/")) {
      targetHost = url.substring(0, url.indexOf("/"));
      documentPath = url.substring(url.indexOf("/"));
    } else {
      targetHost = url;
      documentPath = "/";
    }
  }

  public Request(String url) {
    this(url, 80);
  }

  public String getTargetHost() {
    return this.targetHost;
  }

  public String getUrl() {
    return this.targetHost + documentPath;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer();
    sb.append("GET ")
        .append(documentPath)
        .append(" HTTP/1.1")
        .append(System.lineSeparator())
        .append("host: ")
        .append(targetHost)
        .append(System.lineSeparator())
        .append("Connection: close")
        .append(System.lineSeparator())
        .append(System.lineSeparator());
    return sb.toString();
  }

  public int getPort() {
    return port;
  }
}
