import java.util.Map;

public class Response {
  private String targetHost;
  private String documentPath;
  private int status;
  private String statusMessage;
  private Map<String, String> headers;
  private String body = "";

  public Response(String url) {
    if (url.contains("/")) {
      targetHost = url.substring(0, url.indexOf("/"));
      documentPath = url.substring(url.indexOf("/"));
    } else {
      targetHost = url;
      documentPath = "/";
    }
  }

  public void setStatusFromString(String statusLine) {
    String[] statusArray = statusLine.split(" ", 3);
    status = Integer.parseInt(statusArray[1]);
    statusMessage = statusArray[2];
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public String getBody() {
    return body;
  }

  public String getStatusMessage() {
    return statusMessage;
  }

  public int getStatus() {
    return status;
  }
}
