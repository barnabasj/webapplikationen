import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Connector {
  public static Response send(Request request) {
    Response response = new Response(request.getUrl());

    try (Socket socket = new Socket(request.getTargetHost(), request.getPort())) {
      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
      Scanner in = new Scanner(socket.getInputStream());

      out.println(request);

      String statusLine = in.nextLine();
      response.setStatusFromString(statusLine);

      Map<String, String> headers = parseHeaders(in);
      response.setHeaders(headers);

      String body = parseBody(in);
      response.setBody(body);
    } catch (IOException e) {
      response.setBody("Connection error - can't reach target host");
    }
    return response;
  }

  private static String parseBody(Scanner in) {
    StringBuilder sb = new StringBuilder();
    while (in.hasNext()) {
      sb.append(in.nextLine());
      sb.append(System.lineSeparator());
    }
    return sb.toString().trim();
  }

  private static Map<String, String> parseHeaders(Scanner in) {
    Map<String, String> headers = new HashMap<>();
    String[] headerArray;

    String input = in.nextLine();
    while (input != null && !input.isEmpty()) {
      headerArray = input.split(":", 2);
      headers.put(headerArray[0].trim(), headerArray[1].trim());
      input = in.nextLine();
    }
    return headers;
  }
}
