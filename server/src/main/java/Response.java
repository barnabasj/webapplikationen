import java.util.HashMap;
import java.util.Map;

public class Response {
  private RequestProto proto;
  private int statusCode;
  private String statusMessage;
  private Map<String, String> headers;
  private String body = "";

  public Response(RequestProto proto) {
    this.proto = proto;
    this.headers = new HashMap<>();
  }

  public Map<String, String> getHeaders() {
    return headers;
  }

  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
  }

  public void addHeader(String headerName, String headerValue) {
    this.headers.put(headerName, headerValue);
  }

  public String getStatusMessage() {
    return statusMessage;
  }

  public void setStatusMessage(String statusMessage) {
    this.statusMessage = statusMessage;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  @Override
  public String toString() {
    final StringBuffer sb = new StringBuffer();
    sb.append(RequestProto.toString(this.proto))
        .append(" ")
        .append(this.statusCode)
        .append(" ")
        .append(this.statusMessage)
        .append(System.lineSeparator());

    for (Map.Entry header : headers.entrySet()) {
      sb.append(header.getKey() + ": " + header.getValue());
      sb.append(System.lineSeparator());
    }

    sb.append(System.lineSeparator())
        .append(this.body)
        .append(System.lineSeparator())
        .append(System.lineSeparator());
    return sb.toString();
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }
}
