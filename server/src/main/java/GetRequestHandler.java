import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class GetRequestHandler extends RequestHandler {
  public GetRequestHandler(String root, Request req) {
    super(root, req);
  }

  public Response handle() {
    String path = root + req.getPath();
    if (path.endsWith("/")) {
      path += "index.html";
    }
    System.out.println(path);

    File document = new File(path);
    if (!document.isFile()) {
      res.setStatusCode(404);
      res.setStatusMessage("Not Found");
      return res;
    }

    try (FileReader fr = new FileReader(document)) {
      char[] data = new char[(int) document.length()];
      fr.read(data);
      fr.close();

      res.setBody(String.copyValueOf(data));
      res.setStatusCode(200);
      res.setStatusMessage("OK");
      res.addHeader("Content-Type", "text/html");
    } catch (IOException e) {
      e.printStackTrace();
    }


    return res;
  }
}
