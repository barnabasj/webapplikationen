import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Request {
  private RequestType type;
  private RequestProto protocol;
  private String path;
  private Map headers;
  private String payload = "";

  public Request(String rawRequest) {
    StringTokenizer requestTokenizer = new StringTokenizer(rawRequest, "\n");
    parseRequestLine(requestTokenizer);
    parseHeaders(requestTokenizer);
    parseBody(requestTokenizer);
  }

  public void parseRequestLine(StringTokenizer requestTokenizer) {
    String requestLine = requestTokenizer.nextToken();
    String[] requestArray = requestLine.split(" ", 3);
    type = RequestType.fromString(requestArray[0]);
    path = parseURI(requestArray[1]);
    protocol = RequestProto.fromString(requestArray[2]);
  }

  private String parseURI(String uri) {
    Pattern p = Pattern.compile("^((https?)://)?[\\w|\\.]*(/?.*)");
    Matcher m = p.matcher(uri);
    if (m.find()) {
      return m.group(3);
    } else {
      return "/";
    }
  }

  private void parseHeaders(StringTokenizer requestTokenizer) {
    Map<String, String> headers = new HashMap<>();
    String[] headerArray;
    String line;
    while (requestTokenizer.hasMoreTokens()) {
      line = requestTokenizer.nextToken();

      if (line == null || line.isEmpty()) break;

      headerArray = line.split(":", 2);
      headers.put(headerArray[0].trim(), headerArray[1].trim());
    }
    this.headers = headers;
  }

  private void parseBody(StringTokenizer requestTokenizer) {
    StringBuilder sb = new StringBuilder();
    String line;
    while (requestTokenizer.hasMoreTokens()) {
      line = requestTokenizer.nextToken();
      sb.append(line);
      sb.append(System.lineSeparator());
    }
    this.payload = sb.toString().trim();
  }

  public RequestType getRequestType() {
    return this.type;
  }

  public RequestProto getProtocol() {
    return protocol;
  }

  public String getPath() {
    return path;
  }

  public Map getHeaders() {
    return headers;
  }

  public String getPayload() {
    return payload;
  }
}
