import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

public class Server implements Runnable {

  private int port;
  private boolean listening;
  private String root;
  private ServerSocket serverSocket;
  private List<Thread> requestThreads;


  public Server(int port, String root) {
    this.port = port;
    this.root = root;
    this.requestThreads = new ArrayList();
  }

  public void stop() {
    listening = false;
    try {
      serverSocket.close();
    } catch (IOException e) {

    }
  }

  @Override
  public void run() {
    System.out.println("Server is listening...");
    listening = true;
    try (ServerSocket ss = new ServerSocket(port)) {
      serverSocket = ss;
      while (listening) {
        try {
          Socket connection = ss.accept();
          System.out.println("Received a connection request");
          Thread thread = new Thread(new ConnectionHandler(this, connection));
          thread.start();
          requestThreads.add(thread);
        } catch (SocketException e) {
          stop();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    } catch (IOException e) {
      System.exit(1);
    }
  }

  public String getRoot() {
    return root;
  }
}

