import picocli.CommandLine;

import java.util.Scanner;
import java.util.concurrent.Callable;

@CommandLine.Command(
    description = "Serves files from a given path",
    name = "serve",
    version = "0.1.0")
public class App implements Callable<Void> {
  @CommandLine.Parameters(
      index = "0",
      arity = "0..1",
      description = "Path to root directory from which files will be served")
  private String path = System.getenv("HOME") + "/htdocs";

  @CommandLine.Option(
      names = {"-p", "--port"},
      description = "port the server listens on, default 8080")
  private int port = 8080;

  private Server server;

  public static void main(String[] args) {
    App app = new App();
    CommandLine.call(app, args);
  }

  @Override
  public Void call() {
    this.server = new Server(port, path);
    Thread serverThread = new Thread(this.server);
    serverThread.start();

    Scanner stdIn = new Scanner(System.in);

    String input;
    do {
      System.out.println("Please enter exit to stop the server");
      input = stdIn.nextLine();
    } while (!input.contains("exit"));

    stop();

    return null;
  }

  public void stop() {
    this.server.stop();
  }
}
