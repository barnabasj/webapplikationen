public enum RequestProto {
  HTTP_1_1;

  public static RequestProto fromString(String proto) {
    switch (proto.toLowerCase()) {
      case "http/1.1":
        return HTTP_1_1;
      default:
        throw new IllegalArgumentException("Protocol not supported");
    }
  }

  public static String toString(RequestProto proto) {
    switch (proto) {
      case HTTP_1_1:
        return "HTTP/1.1";
      default:
        throw new IllegalArgumentException("Protocol not supported");
    }
  }
}
