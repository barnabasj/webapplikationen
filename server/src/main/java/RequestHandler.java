public abstract class RequestHandler {
  protected String root;
  protected Request req;
  protected Response res;

  protected RequestHandler(String root, Request req) {
    this.root = root;
    this.req = req;
    this.res = new Response(req.getProtocol());
  }

  public static RequestHandler getHandler(Request req, String root) {
    switch (req.getRequestType()) {
      case GET:
        return new GetRequestHandler(root, req);
      default:
        throw new IllegalArgumentException("Invalid Request");
    }
  }

  public abstract Response handle();
}
