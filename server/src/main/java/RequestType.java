public enum RequestType {
  GET;

  public static RequestType fromString(String s) {
    switch (s.toLowerCase()) {
      case "get":
        return GET;
      default:
        throw new IllegalArgumentException("Request Type is not supported");
    }
  }
}
