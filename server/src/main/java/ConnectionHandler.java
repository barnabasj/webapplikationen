import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionHandler implements Runnable {
  private final Socket connection;
  private final Server server;

  public ConnectionHandler(Server server, Socket connection) {
    this.server = server;
    this.connection = connection;
  }

  @Override
  public void run() {
    System.out.println("Handling new Request");
    try {
      PrintWriter out = new PrintWriter(connection.getOutputStream(), true);
      BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

      StringBuilder rawRequestBuilder = new StringBuilder();
      String line;
      do {
        line = in.readLine();
        if (line.equals("")) break;
        rawRequestBuilder.append(line);
        rawRequestBuilder.append(System.lineSeparator());
      }
      while (true);

      Request req = new Request(rawRequestBuilder.toString());
      RequestHandler reqHandler = RequestHandler.getHandler(req, server.getRoot());
      Response res = reqHandler.handle();

      out.println(res);
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      connection.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
